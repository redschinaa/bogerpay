**BogerPay Application****


**User Accounts**

- default user accounts were created for the team members:
    - boger@bp.de, klamer@bp.de, greim@bp.de, richter@bp.de / password for all users: 1234



**Bank Accounts**

- each bank account gets 10.000,00 € as standard
- since the authenticity of bank bank accounts cannot be validated / verified as with conventional payment providers such as paypal, I assume that a user does not create an infinite number of bank accounts


**Transactions**

- The transaction takes place via a REST interface. A DTO with the necessary information has to be transfered via POST request.
- In order to simplify the process only the consignor's IBAN is checked for its existence, since the recipient could also have an bank account at another payment providor


**Bank Statements**

- a bank statement request triggers the delivery service of the DeliveryGreim application, the delivery number in the transaction overview can be used to track the delivery on the DeliveryGreim site
