package de.othr.bor.bogerpay.services.BankAccount;

public class BankAccountNotFoundException extends RuntimeException {

    BankAccountNotFoundException(String errorMessage) {
        super(errorMessage);
    }

}
