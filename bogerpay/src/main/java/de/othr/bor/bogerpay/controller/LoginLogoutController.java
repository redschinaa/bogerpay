package de.othr.bor.bogerpay.controller;

import de.othr.bor.bogerpay.services.UserAccount.UserAccountServiceIF;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;

@Controller
public class LoginLogoutController {


    @RequestMapping("/")
    public String start() {
        return "login";
    }


    @RequestMapping(value="/login")
    public String login(Model model) {

        model.addAttribute("loginError", true);
        return "login";

    }


    @RequestMapping("/logout")
    public String logout() {
        return "login";
    }


}
