package de.othr.bor.bogerpay.services.BankAccount;


import de.othr.bor.bogerpay.entity.BankAccount;
import de.othr.bor.bogerpay.entity.UserAccount;
import de.othr.bor.bogerpay.repository.BankAccountRepository;
import de.othr.bor.bogerpay.repository.UserAccountRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;
import java.util.Optional;

@Service
@Qualifier("labresources")
public class BankAccountService implements BankAccountServiceIF {


    @Autowired
    private BankAccountRepository bankAccountRepository;

    @Autowired
    private UserAccountRepository userAccountRepository;



    @Override
    @Transactional
    public BankAccount createBankAccount(BankAccount bankAccount) throws BankAccountAlreadyExistsException {

        Optional<BankAccount> optional = bankAccountRepository.findById(bankAccount.getIBAN());

        if(optional.isPresent()) {
            throw new BankAccountAlreadyExistsException("Bank account with IBAN" + bankAccount.getIBAN() + "already exists!");
        }

        bankAccount.getBalanceFormatted();
        bankAccount.setBankBalance(10000);
        bankAccount = bankAccountRepository.save(bankAccount);

        return bankAccount;

    }



    @Override
    public List<BankAccount> getAllBankAccounts(String currentUserAccountID) throws UsernameNotFoundException {

        Optional<UserAccount> optional = userAccountRepository.findById(currentUserAccountID);

        if(optional.isPresent()) {

            UserAccount userAccount = optional.get();
            return userAccount.getBankAccountsList();

        }

        throw new UsernameNotFoundException("User account with email" + currentUserAccountID + "could not be found!");

    }



    @Override
    public BankAccount getBankAccountByIBAN(String IBAN) throws BankAccountNotFoundException {

        return bankAccountRepository.findBankAccountByIBAN(IBAN)
                .orElseThrow( () -> {
                    throw new BankAccountNotFoundException("Bank account with IBAN " + IBAN + "could not be found!");
                });

    }



    @Override
    @Transactional(propagation = Propagation.REQUIRED)
    public void archiveBankAccount(BankAccount bankAccount) {

        bankAccount.setArchived(true);
        bankAccountRepository.save(bankAccount);

    }

}
