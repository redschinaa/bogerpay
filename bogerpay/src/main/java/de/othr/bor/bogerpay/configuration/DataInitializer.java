package de.othr.bor.bogerpay.configuration;

import de.othr.bor.bogerpay.entity.Address;
import de.othr.bor.bogerpay.entity.BankAccount;
import de.othr.bor.bogerpay.entity.UserAccount;
import de.othr.bor.bogerpay.repository.UserAccountRepository;
import de.othr.bor.bogerpay.services.BankAccount.BankAccountAlreadyExistsException;
import de.othr.bor.bogerpay.services.BankAccount.BankAccountServiceIF;
import de.othr.bor.bogerpay.services.UserAccount.AccountAlreadyExistsException;
import de.othr.bor.bogerpay.services.UserAccount.UserAccountServiceIF;
import org.springframework.boot.context.event.ApplicationReadyEvent;
import org.springframework.context.event.EventListener;
import org.springframework.stereotype.Component;
import org.springframework.transaction.annotation.Transactional;

@Component
public class DataInitializer {

    private final UserAccountServiceIF userAccountService;
    private final BankAccountServiceIF bankAccountService;
    private final UserAccountRepository userAccountRepository;


    public DataInitializer(UserAccountServiceIF userAccountService, BankAccountServiceIF bankAccountService, UserAccountRepository userAccountRepository) {

        this.userAccountService = userAccountService;
        this.bankAccountService = bankAccountService;
        this.userAccountRepository = userAccountRepository;

    }


    // Create default UserAccounts for Team Members, if not present
    @EventListener(ApplicationReadyEvent.class)
    @Transactional
    public void initializeUserAccounts() throws Exception {
        long numberOfAccounts = userAccountRepository.count();

        if(numberOfAccounts < 5) {
            createBogerPay();
            createBambooForestStore();
            createDeliveryGreim();
            createSixxr();
            createBambooForestStoreUser();
        }

    }


    @Transactional
    protected void createBogerPay() throws AccountAlreadyExistsException, BankAccountAlreadyExistsException {

        UserAccount userAccount = new UserAccount();
        userAccount.setFormOfAddress("Mrs");
        userAccount.setFirstname("Regina");
        userAccount.setLastname("Boger");
        userAccount.setEmail("boger@bp.de");
        userAccount.setPassword("1234");

        Address address = new Address();
        address.setStreet("Oberländerstraße");
        address.setHouseNumber("14");
        address.setPostCode("93051");
        address.setCity("Regensburg");
        address.setCountry("Deutschland");
        userAccount.setAddress(address);
        userAccountService.createUserAccount(userAccount);

        BankAccount bankAccount = new BankAccount();
        bankAccount.setIBAN("DE50750500000000000001");
        bankAccount.setBIC("BYLADEM1RGB");
        bankAccountService.createBankAccount(bankAccount);
        userAccountService.addBankAccountToUserAccount(userAccount.getEmail(), bankAccount);

    }

    @Transactional
    protected void createBambooForestStore() throws AccountAlreadyExistsException, BankAccountAlreadyExistsException {

        UserAccount userAccount = new UserAccount();
        userAccount.setFormOfAddress("Mr");
        userAccount.setFirstname("Florian");
        userAccount.setLastname("Klamer");
        userAccount.setEmail("klamer@bp.de");
        userAccount.setPassword("1234");

        Address address = new Address();
        address.setStreet("Brunhuberstraße");
        address.setHouseNumber("4D");
        address.setPostCode("93051");
        address.setCity("Regensburg");
        address.setCountry("Deutschland");
        userAccount.setAddress(address);
        userAccountService.createUserAccount(userAccount);

        BankAccount bankAccount = new BankAccount();
        bankAccount.setIBAN("DE50750500000000000002");
        bankAccount.setBIC("BYLADEM1RGB");
        bankAccountService.createBankAccount(bankAccount);
        userAccountService.addBankAccountToUserAccount(userAccount.getEmail(), bankAccount);

    }

    @Transactional
    protected void createDeliveryGreim() throws AccountAlreadyExistsException,BankAccountAlreadyExistsException {

        UserAccount userAccount = new UserAccount();
        userAccount.setFormOfAddress("Mrs");
        userAccount.setFirstname("Katharina");
        userAccount.setLastname("Greim");
        userAccount.setEmail("greim@bp.de");
        userAccount.setPassword("1234");

        Address address = new Address();
        address.setStreet("Brunhuberstraße");
        address.setHouseNumber("4D");
        address.setPostCode("93051");
        address.setCity("Regensburg");
        address.setCountry("Deutschland");
        userAccount.setAddress(address);
        userAccountService.createUserAccount(userAccount);

        BankAccount bankAccount = new BankAccount();
        bankAccount.setIBAN("DE50750500000000000003");
        bankAccount.setBIC("BYLADEM1RGB");
        bankAccountService.createBankAccount(bankAccount);
        userAccountService.addBankAccountToUserAccount(userAccount.getEmail(), bankAccount);

    }

    @Transactional
    protected void createSixxr() throws AccountAlreadyExistsException, BankAccountAlreadyExistsException {

        UserAccount userAccount = new UserAccount();
        userAccount.setFormOfAddress("Mr");
        userAccount.setFirstname("Felix");
        userAccount.setLastname("Richter");
        userAccount.setEmail("richter@bp.de");
        userAccount.setPassword("1234");

        Address address = new Address();
        address.setStreet("Florastraße");
        address.setHouseNumber("24");
        address.setPostCode("92421");
        address.setCity("Schwandorf");
        address.setCountry("Deutschland");
        userAccount.setAddress(address);
        userAccountService.createUserAccount(userAccount);

        BankAccount bankAccount = new BankAccount();
        bankAccount.setIBAN("DE50750500000000000004");
        bankAccount.setBIC("BYLADEM1SAD");
        bankAccountService.createBankAccount(bankAccount);
        userAccountService.addBankAccountToUserAccount(userAccount.getEmail(), bankAccount);

    }

    @Transactional
    protected void createBambooForestStoreUser() throws AccountAlreadyExistsException, BankAccountAlreadyExistsException {

        UserAccount userAccount = new UserAccount();
        userAccount.setFormOfAddress("Mr");
        userAccount.setFirstname("Thomas");
        userAccount.setLastname("Test");
        userAccount.setEmail("BambooForestStoreUser@bp.de");
        userAccount.setPassword("1234");

        Address address = new Address();
        address.setStreet("Teststraße");
        address.setHouseNumber("1");
        address.setPostCode("93051");
        address.setCity("Regensburg");
        address.setCountry("Deutschland");
        userAccount.setAddress(address);
        userAccountService.createUserAccount(userAccount);

        BankAccount bankAccount = new BankAccount();
        bankAccount.setIBAN("DE50750500000000000007");
        bankAccount.setBIC("BYLADEM1RGB");
        bankAccountService.createBankAccount(bankAccount);
        userAccountService.addBankAccountToUserAccount(userAccount.getEmail(), bankAccount);

    }

}
