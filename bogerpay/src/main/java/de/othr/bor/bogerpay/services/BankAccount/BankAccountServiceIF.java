package de.othr.bor.bogerpay.services.BankAccount;

import de.othr.bor.bogerpay.entity.BankAccount;
import org.springframework.security.core.userdetails.UsernameNotFoundException;

import java.util.List;

public interface BankAccountServiceIF {

    BankAccount createBankAccount(BankAccount bankAccount) throws BankAccountAlreadyExistsException;

    BankAccount getBankAccountByIBAN(String IBAN) throws BankAccountNotFoundException;

    List<BankAccount> getAllBankAccounts(String currentUserAccountID) throws UsernameNotFoundException;

    void archiveBankAccount(BankAccount bankAccount);

}
