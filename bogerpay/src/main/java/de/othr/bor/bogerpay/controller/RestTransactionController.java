package de.othr.bor.bogerpay.controller;

import de.othr.bor.bogerpay.dto.TransactionDTO;
import de.othr.bor.bogerpay.entity.UserAccount;
import de.othr.bor.bogerpay.services.Transaction.TransactionException;
import de.othr.bor.bogerpay.services.Transaction.TransactionServiceIF;
import de.othr.bor.bogerpay.services.UserAccount.UserAccountServiceIF;
import org.springframework.web.bind.annotation.*;


@RestController
public class RestTransactionController {

    private final UserAccountServiceIF userAccountService;
    private final TransactionServiceIF transactionService;

    public RestTransactionController(UserAccountServiceIF userAccountService, TransactionServiceIF transactionService) {

        this.userAccountService = userAccountService;
        this.transactionService = transactionService;

    }


    @PostMapping("/api/transaction")
    @ResponseBody
    public TransactionDTO newTransaction(

            @RequestBody TransactionDTO transactionDTO,
            @RequestParam() String email,
            @RequestParam() String password

    ) throws TransactionException {

        UserAccount account = userAccountService.validateCredentials(email, password);

        String formatedConsignorIBAN = transactionDTO.getConsignorIBAN().replaceAll("\\s","");
        transactionDTO.setConsignorIBAN(formatedConsignorIBAN);
        String formatedRecipientIBAN = transactionDTO.getRecipientIBAN().replaceAll("\\s","");
        transactionDTO.setRecipientIBAN(formatedRecipientIBAN);


        if (account == null || !transactionService.isOwner(account.getEmail(), transactionDTO.getConsignorIBAN())) {
            throw new TransactionException("User can't authenticate!");
        }

        return transactionService.validateTransactionRequest(transactionDTO);

    }

}
