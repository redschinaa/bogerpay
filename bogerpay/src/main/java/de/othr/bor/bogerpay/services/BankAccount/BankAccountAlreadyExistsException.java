package de.othr.bor.bogerpay.services.BankAccount;

public class BankAccountAlreadyExistsException extends Exception {

    BankAccountAlreadyExistsException(String errorMessage) {
        super(errorMessage);
    }

}
