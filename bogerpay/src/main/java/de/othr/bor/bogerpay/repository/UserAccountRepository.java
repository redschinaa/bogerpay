package de.othr.bor.bogerpay.repository;

import de.othr.bor.bogerpay.entity.UserAccount;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface UserAccountRepository extends CrudRepository<UserAccount, String> {
}
