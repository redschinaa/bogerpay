package de.othr.bor.bogerpay.controller;

import de.othr.bor.bogerpay.dto.TransactionDTO;
import de.othr.bor.bogerpay.entity.BankAccount;
import de.othr.bor.bogerpay.entity.Transaction;
import de.othr.bor.bogerpay.entity.UserAccount;
import de.othr.bor.bogerpay.services.BankAccount.BankAccountServiceIF;
import de.othr.bor.bogerpay.services.Transaction.TransactionServiceIF;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.annotation.AuthenticationPrincipal;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.servlet.mvc.support.RedirectAttributes;

import java.security.Principal;
import java.util.Collection;
import java.util.List;

@Controller
public class TransactionController {

    @Autowired
    private TransactionServiceIF transactionService;

    @Autowired
    private BankAccountServiceIF bankAccountService;



    @RequestMapping(value = "/transaction")
    public String transactionBankAccount(
            Principal principal,
            Model model
    ) {

        String currentUserAccountID = principal.getName();
        List<BankAccount> bankAccountList = bankAccountService.getAllBankAccounts(currentUserAccountID);

        model.addAttribute("bankAccountsList", bankAccountList);
        model.addAttribute("transactionData", new TransactionDTO());

        for(BankAccount bankAccount : bankAccountList) {

            Collection<Transaction> transactionsList = transactionService.getAllTransactions(bankAccount);
            model.addAttribute("transactionsList", transactionsList);

        }

        return "transaction";

    }


    @RequestMapping(value = "/transaction/execute", method = RequestMethod.POST)
    public String createTransaction(

            @ModelAttribute("consignorIBAN") String consignorIBAN,
            @ModelAttribute("recipientName") String recipientName,
            @ModelAttribute("recipientIBAN") String recipientIBAN,
            @ModelAttribute("amount") double amount,
            @ModelAttribute("reference") String reference,
            @AuthenticationPrincipal UserAccount principal,
            RedirectAttributes redirectAttributes
    ) {
        try {

            String currentUserAccountID = principal.getEmail();

            if(!transactionService.isOwner(currentUserAccountID, consignorIBAN)){

                redirectAttributes.addFlashAttribute(("ibanNotFound"), true);
                return "redirect:/transaction";

            }

            TransactionDTO transactionDTO = new TransactionDTO();

            String formatedConsignorIBAN = consignorIBAN.replaceAll("\\s","");
            transactionDTO.setConsignorIBAN(formatedConsignorIBAN);
            String formatedRecipientIBAN = recipientIBAN.replaceAll("\\s","");
            transactionDTO.setRecipientIBAN(formatedRecipientIBAN);

            if((transactionDTO.getConsignorIBAN().equals(transactionDTO.getRecipientIBAN()))) {

                redirectAttributes.addFlashAttribute("sameIBANError", true);
                return "redirect:/transaction";

            }

            transactionDTO.setRecipientName(recipientName);
            transactionDTO.setAmount(amount);
            transactionDTO.setReference(reference);
            transactionService.validateTransactionRequest(transactionDTO);


        } catch (Exception e) {
            redirectAttributes.addFlashAttribute("creationFail", true);
        }

        redirectAttributes.addFlashAttribute("creationSuccess", true);
        return "redirect:/transaction";

    }


}
