package de.othr.bor.bogerpay.repository;

import de.othr.bor.bogerpay.entity.BankAccount;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

import java.util.Optional;

@Repository
public interface BankAccountRepository extends CrudRepository<BankAccount, String> {

    Optional<BankAccount> findBankAccountByIBAN(String IBAN);

}
