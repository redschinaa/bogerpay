package de.othr.bor.bogerpay.services.UserAccount;

import de.othr.bor.bogerpay.entity.BankAccount;
import de.othr.bor.bogerpay.entity.UserAccount;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UsernameNotFoundException;

public interface UserAccountServiceIF {

    UserAccount createUserAccount(UserAccount userAccount) throws AccountAlreadyExistsException;

    UserAccount getUserAccountByID(String email) throws AccountNotFoundException;

    void updateUserAccount(UserAccount userAccount);

    void addBankAccountToUserAccount(String currentUserAccountID, BankAccount newBankAccount) throws UsernameNotFoundException;

    UserAccount validateCredentials(String email, String password);

    UserDetails loadUserByUsername(String email) throws UsernameNotFoundException;

}
