package de.othr.bor.bogerpay.services.Transaction;

import de.othr.bor.bogerpay.dto.TransactionDTO;
import de.othr.bor.bogerpay.entity.BankAccount;
import de.othr.bor.bogerpay.entity.Transaction;
import de.othr.bor.bogerpay.entity.UserAccount;
import de.othr.bor.bogerpay.repository.BankAccountRepository;
import de.othr.bor.bogerpay.repository.TransactionRepository;
import de.othr.bor.bogerpay.repository.UserAccountRepository;
import de.othr.bor.bogerpay.services.BankAccount.BankAccountServiceIF;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.Calendar;
import java.util.Collection;
import java.util.Optional;

@Service
@Qualifier("labresources")
public class TransactionService implements TransactionServiceIF {


    @Autowired
    private TransactionRepository transactionRepository;

    @Autowired
    private BankAccountRepository bankAccountRepository;

    @Autowired
    private UserAccountRepository userAccountRepository;

    @Autowired
    private BankAccountServiceIF bankAccountService;



    // Check if Consignor has bank account with chosen IBAN
    @Override
    public boolean isOwner(String email, String IBAN) {

        Optional<UserAccount> optional = userAccountRepository.findById(email);

        if(optional.isPresent()) {

            UserAccount userAccount = optional.get();

            return userAccount.getBankAccountsList().stream().map(BankAccount::getIBAN).filter(IBAN::equals).findFirst().isPresent();

        }

        return false;

    }



    @Override
    public Transaction createTransaction(TransactionDTO transactionDTO, BankAccount consignor, BankAccount recipient) {

        Transaction transaction = new Transaction();

        transaction.setConsignor(consignor);
        transaction.setRecipient(recipient);
        transaction.setReference(transactionDTO.getReference());
        transaction.setRecipientName(transactionDTO.getRecipientName());

        transaction.setAmount(transactionDTO.getAmount());

        return transaction;

    }



    @Transactional
    protected void executeTransaction(Transaction transaction) throws TransactionException {

        try {

            BankAccount consignor = bankAccountService.getBankAccountByIBAN(transaction.getConsignor().getIBAN());
            BankAccount recipient = bankAccountService.getBankAccountByIBAN(transaction.getRecipient().getIBAN());

            if(consignor.getBankBalance() - transaction.getAmount() < consignor.getCreditLimit()) {

                throw new TransactionException("Bank account with IBAN " + consignor.getIBAN() + " is not funded");
            }

            transaction.setTimestamp(Calendar.getInstance().getTime());

            consignor.setBankBalance(consignor.getBankBalance() - transaction.getAmount());
            recipient.setBankBalance(recipient.getBankBalance() + transaction.getAmount());


            transactionRepository.save(transaction);

            bankAccountRepository.save(consignor);
            bankAccountRepository.save(recipient);


        } catch (UsernameNotFoundException e) {
            throw new TransactionException(e.getMessage());
        }

    }



    @Override
    public TransactionDTO validateTransactionRequest(TransactionDTO transactionDTO) throws TransactionException {

        try {

            BankAccount consignor = bankAccountService.getBankAccountByIBAN(transactionDTO.getConsignorIBAN());
            BankAccount recipient = bankAccountService.getBankAccountByIBAN(transactionDTO.getRecipientIBAN());


            Transaction transaction = createTransaction(transactionDTO, consignor, recipient);

            executeTransaction(transaction);

            return transactionDTO;

        } catch(UsernameNotFoundException e) {
            throw new TransactionException(e.getMessage());
        }

    }



    @Override
    @Transactional
    public Collection<Transaction> getAllTransactions(BankAccount bankAccount) {

        return transactionRepository.getAllTransactions(bankAccount);

    }



    @Override
    public void deleteBankAccountTransaction(TransactionDTO transactionDTO) throws TransactionException {

        try {

            BankAccount consignor = bankAccountService.getBankAccountByIBAN(transactionDTO.getConsignorIBAN());
            BankAccount recipient = bankAccountService.getBankAccountByIBAN(transactionDTO.getRecipientIBAN());

            consignor.setArchived(true);

            Transaction transaction = createTransaction(transactionDTO, consignor, recipient);

            executeTransaction(transaction);


        } catch (TransactionException e) {
            throw  new TransactionException(e.getMessage());
        }

    }

}
