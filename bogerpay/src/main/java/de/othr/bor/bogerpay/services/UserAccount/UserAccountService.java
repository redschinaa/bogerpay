package de.othr.bor.bogerpay.services.UserAccount;

import de.othr.bor.bogerpay.entity.BankAccount;
import de.othr.bor.bogerpay.entity.UserAccount;
import de.othr.bor.bogerpay.repository.UserAccountRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.stereotype.Service;

import java.util.Optional;

@Service
@Qualifier("labresources")
public class UserAccountService implements UserAccountServiceIF, UserDetailsService {

    @Autowired
    private UserAccountRepository userAccountRepository;


    @Autowired
    private BCryptPasswordEncoder passwordEncoder;



    @Override
    public UserAccount createUserAccount(UserAccount userAccount) throws AccountAlreadyExistsException {

        Optional<UserAccount> optional = userAccountRepository.findById(userAccount.getEmail());

        if (optional.isPresent()) {
            throw new AccountAlreadyExistsException("Account with Email " + userAccount.getEmail() + "already exists!");
        }

        userAccount.setPassword(passwordEncoder.encode(userAccount.getPassword()));

        userAccount = userAccountRepository.save(userAccount);

        return userAccount;

    }


    @Override
    public void addBankAccountToUserAccount(String currentUserAccountID, BankAccount newBankAccount) throws UsernameNotFoundException {

        Optional<UserAccount> optional = userAccountRepository.findById(currentUserAccountID);

        if (optional.isPresent()) {

            UserAccount userAccount = optional.get();

            userAccount.addBankAccountToList(newBankAccount);
            userAccountRepository.save(userAccount);

            return;
        }

        throw new UsernameNotFoundException("User account with email" + currentUserAccountID + "could not be found!");

    }


    @Override
    public UserAccount validateCredentials(String email, String password) {

        Optional<UserAccount> optional = userAccountRepository.findById(email);

        if (!optional.isPresent()) {
            return null;
        }

        UserAccount userAccount = optional.get();

        String userPwd = userAccount.getPassword();

        if(!passwordEncoder.matches(password, userPwd)) {
            return null;
        }

        return userAccount;

    }


    @Override
    public void updateUserAccount(UserAccount userAccount) {

        userAccountRepository.save(userAccount);

    }


    @Override
    public UserAccount getUserAccountByID(String email) {

        Optional<UserAccount> optional = userAccountRepository.findById(email);
        if (optional.isPresent()) {
            UserAccount userAccount = optional.get();
            return userAccount;
        }
        throw new AccountNotFoundException("User account with email" + email + "could not be found!");

    }


    @Override
    public UserDetails loadUserByUsername(String email) throws UsernameNotFoundException {

        Optional<UserAccount> optional = userAccountRepository.findById(email);

        if (optional.isPresent()) {
            UserAccount userAccount = optional.get();
            return userAccount;
        }
        throw new UsernameNotFoundException("Account with Email" + email + "does not exist!");

    }

}
