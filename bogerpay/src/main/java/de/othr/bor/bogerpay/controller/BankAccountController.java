package de.othr.bor.bogerpay.controller;

import de.othr.bor.bogerpay.dto.TransactionDTO;
import de.othr.bor.bogerpay.entity.BankAccount;
import de.othr.bor.bogerpay.entity.UserAccount;
import de.othr.bor.bogerpay.services.BankAccount.BankAccountAlreadyExistsException;
import de.othr.bor.bogerpay.services.BankAccount.BankAccountServiceIF;
import de.othr.bor.bogerpay.services.Transaction.TransactionException;
import de.othr.bor.bogerpay.services.Transaction.TransactionServiceIF;
import de.othr.bor.bogerpay.services.UserAccount.UserAccountServiceIF;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.servlet.mvc.support.RedirectAttributes;

import java.security.Principal;
import java.util.List;


@Controller
public class BankAccountController {

    @Autowired
    private BankAccountServiceIF bankAccountService;

    @Autowired
    private UserAccountServiceIF userAccountService;

    @Autowired
    private TransactionServiceIF transactionService;



    @RequestMapping(value = "/bankAccount", method = RequestMethod.GET)
    public String listBankAccounts(

            Principal principal,
            Model model

    ) {

        String currentUserAccountID = principal.getName();
        List<BankAccount> bankAccountsList = bankAccountService.getAllBankAccounts(currentUserAccountID);

        model.addAttribute("bankAccountsList", bankAccountsList);
        return "bankAccount";

    }



    @RequestMapping(value = "/bankAccount/create", method = RequestMethod.POST)
    public String createBankAccount(

            @ModelAttribute("iban") String iban,
            @ModelAttribute("bic") String bic,
            Principal principal,
            RedirectAttributes redirectAttributes

    ) {
        try {

            String currentUserAccountID = principal.getName();

            BankAccount bankAccount = new BankAccount();
            String formatedIBAN = iban.replaceAll("\\s","");
            bankAccount.setIBAN(formatedIBAN);
            bankAccount.setBIC(bic);
            bankAccount = bankAccountService.createBankAccount(bankAccount);
            userAccountService.addBankAccountToUserAccount(currentUserAccountID, bankAccount);

        } catch (BankAccountAlreadyExistsException e) {
            redirectAttributes.addFlashAttribute("ibanAlreadyExists", true);
        }

        redirectAttributes.addFlashAttribute("creationSuccess", true);
        return "redirect:/bankAccount";

    }



    @RequestMapping(value = "/bankAccount/delete", method = RequestMethod.POST)
    public String deleteBankAccount(

            @ModelAttribute("transferIBAN") String transferIBAN,
            @ModelAttribute("deleteIBAN") String deleteIBAN,
            @ModelAttribute("amount") double amount,
            @ModelAttribute("reference") String reference,
            Principal principal,
            RedirectAttributes redirectAttributes

    ) {

        try {

            String currentUserAccountID = principal.getName();
            UserAccount userAccount= userAccountService.getUserAccountByID(currentUserAccountID);

            TransactionDTO transactionDTO = new TransactionDTO();

            transactionDTO.setRecipientIBAN(transferIBAN);
            transactionDTO.setConsignorIBAN(deleteIBAN);
            transactionDTO.setAmount(amount);
            transactionDTO.setReference(reference);
            transactionDTO.setRecipientName(userAccount.getFirstname() + " " + userAccount.getLastname());

            if(!(transactionDTO.getRecipientIBAN().equals(transactionDTO.getConsignorIBAN()))) {

                transactionService.deleteBankAccountTransaction(transactionDTO);
                BankAccount bankAccount = bankAccountService.getBankAccountByIBAN(transactionDTO.getConsignorIBAN());
                bankAccountService.archiveBankAccount(bankAccount);

            } else {
                throw new TransactionException("A different bank account to transfer the money has to be selected!");
            }

        } catch (TransactionException e) {
            redirectAttributes.addFlashAttribute("deleteError", e.getMessage());
        }

        return "redirect:/bankAccount";

    }

}
