package de.othr.bor.bogerpay.repository;


import de.othr.bor.bogerpay.entity.BankStatement;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface BankStatementRepository extends CrudRepository<BankStatement, Long> {
}
