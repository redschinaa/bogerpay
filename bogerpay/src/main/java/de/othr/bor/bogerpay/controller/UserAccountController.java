package de.othr.bor.bogerpay.controller;

import de.othr.bor.bogerpay.entity.Address;
import de.othr.bor.bogerpay.entity.UserAccount;
import de.othr.bor.bogerpay.services.UserAccount.AccountAlreadyExistsException;
import de.othr.bor.bogerpay.services.UserAccount.UserAccountServiceIF;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.servlet.mvc.support.RedirectAttributes;

import java.security.Principal;

@Controller
public class UserAccountController {

    @Autowired
    private UserAccountServiceIF userAccountService;



    @RequestMapping("/register")
    public  String register() { return "register"; }



    @RequestMapping(value="/createAccount", method = RequestMethod.POST)
    public String createAccount(

            @ModelAttribute("formOfAddress") String formOfAddress,
            @ModelAttribute("firstname") String firstname,
            @ModelAttribute("lastname") String lastname,
            @ModelAttribute("street") String street,
            @ModelAttribute("houseNumber") String houseNumber,
            @ModelAttribute("city") String city,
            @ModelAttribute("postCode") String postCode,
            @ModelAttribute("country") String country,
            @ModelAttribute("email") String email,
            @ModelAttribute("password") String password,
            Model model
    ) {
        try {

            UserAccount userAccount = new UserAccount();

            userAccount.setFormOfAddress(formOfAddress);
            userAccount.setFirstname(firstname);
            userAccount.setLastname(lastname);
            userAccount.setEmail(email);
            userAccount.setPassword(password);
            Address address = new Address();
            address.setStreet(street);
            address.setHouseNumber(houseNumber);
            address.setCity(city);
            address.setPostCode(postCode);
            address.setCountry(country);
            userAccount.setAddress(address);

            userAccountService.createUserAccount(userAccount);

        } catch (AccountAlreadyExistsException e) {
            model.addAttribute("emailError", true);
            return "register";
        }

        model.addAttribute("creationSuccess", true);
        return "login";

    }



    @RequestMapping("/overview")
    public String viewOverview(

            Principal principal,
            Model model
    ) {

        UserAccount userAccount = userAccountService.getUserAccountByID(principal.getName());
        model.addAttribute("userAccount", userAccount);
        return "overview";

    }



    @RequestMapping(value="/userAccount/edit", method = RequestMethod.POST)
    public String editAddress(

            @ModelAttribute("street") String street,
            @ModelAttribute("houseNumber") String houseNumber,
            @ModelAttribute("city") String city,
            @ModelAttribute("postCode") String postCode,
            @ModelAttribute("country") String country,
            @ModelAttribute("email") String email,
            @ModelAttribute("password") String password,
            Principal principal,
            RedirectAttributes redirectAttributes
    ) {

        String currentUserAccountID = principal.getName();
        UserAccount userAccount = userAccountService.getUserAccountByID(currentUserAccountID);

        Address newAddress = userAccount.getAddress();
        newAddress.setStreet(street);
        newAddress.setHouseNumber(houseNumber);
        newAddress.setCity(city);
        newAddress.setPostCode(postCode);
        newAddress.setCountry(country);
        userAccount.setAddress(newAddress);

        userAccountService.updateUserAccount(userAccount);

        redirectAttributes.addFlashAttribute("editSuccess", true);
        return "redirect:/overview";

    }

}
