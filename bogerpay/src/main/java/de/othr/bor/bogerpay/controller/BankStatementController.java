package de.othr.bor.bogerpay.controller;

import de.othr.bor.bogerpay.entity.BankStatement;
import de.othr.bor.bogerpay.entity.UserAccount;
import de.othr.bor.bogerpay.services.BankStatement.BankStatementServiceIF;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.annotation.AuthenticationPrincipal;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

@Controller
public class BankStatementController {

    @Autowired
    private BankStatementServiceIF bankStatementService;


    @RequestMapping(value = "/bankStatement/request", method = RequestMethod.POST)
    public String requestBankStatement(

            @ModelAttribute("bankStatementIBAN") String bankStatementIBAN,
            @ModelAttribute("documentOption") String documentOption,
            @ModelAttribute("deliverOption") String deliverOption,
            @AuthenticationPrincipal UserAccount principal

    ) {

        try{

            String currentUserAccountID = principal.getEmail();

            BankStatement bankStatement = new BankStatement();

            bankStatement.setBankStatementIBAN(bankStatementIBAN);
            bankStatement.setDocumentOption(documentOption);
            bankStatement.setDeliverOption(deliverOption);

            bankStatementService.requestBankStatement(currentUserAccountID, bankStatement);


        } catch (Exception e) {
            e.printStackTrace();
        }

        return "redirect:/transaction";

    }

}
