package de.othr.bor.bogerpay.repository;

import de.othr.bor.bogerpay.entity.BankAccount;
import de.othr.bor.bogerpay.entity.Transaction;
import de.othr.bor.bogerpay.entity.UserAccount;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

import java.util.Collection;

@Repository
public interface TransactionRepository extends CrudRepository<Transaction, Long> {

    // Due to the duplicate Relationship between Transaction and BankAccount
    // I decided not to use bidirectional mapping and rather query one side

    @Query("SELECT transaction FROM Transaction transaction WHERE transaction.consignor=?1 OR transaction.recipient=?1 ORDER BY transaction.timestamp DESC")
    Collection<Transaction> getAllTransactions(BankAccount bankAccount);

}
