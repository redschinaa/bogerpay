package de.othr.bor.bogerpay.services.Transaction;

import de.othr.bor.bogerpay.dto.TransactionDTO;
import de.othr.bor.bogerpay.entity.BankAccount;
import de.othr.bor.bogerpay.entity.Transaction;

import java.util.Collection;


public interface TransactionServiceIF {

    Transaction createTransaction(TransactionDTO transactionDTO, BankAccount consignor, BankAccount recipient);

    TransactionDTO validateTransactionRequest(TransactionDTO transactionDTO) throws TransactionException;

    void deleteBankAccountTransaction(TransactionDTO transactionData) throws TransactionException;

    boolean isOwner(String email, String IBAN);

    Collection<Transaction> getAllTransactions(BankAccount bankAccount);

}
