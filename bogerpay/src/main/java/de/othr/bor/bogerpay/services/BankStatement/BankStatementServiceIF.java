package de.othr.bor.bogerpay.services.BankStatement;

import de.othr.bor.bogerpay.entity.BankStatement;

public interface BankStatementServiceIF {

    void requestBankStatement(String currentUserAccountID, BankStatement bankStatement);

}
