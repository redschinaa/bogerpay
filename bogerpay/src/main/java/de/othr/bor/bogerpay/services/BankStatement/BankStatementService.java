package de.othr.bor.bogerpay.services.BankStatement;


import de.othr.bor.bogerpay.entity.BankStatement;
import de.othr.bor.bogerpay.entity.UserAccount;
import de.othr.bor.bogerpay.repository.UserAccountRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;
import org.springframework.web.client.RestTemplate;
import oth.greim.DeliveryGreim.DTOs.DeliveryDTO;
import oth.greim.DeliveryGreim.DTOs.DeliveryObjectDTO;
import oth.greim.DeliveryGreim.Entitys.Address;

import java.util.Optional;

import static oth.greim.DeliveryGreim.Entitys.Category.LargeDocument;
import static oth.greim.DeliveryGreim.Entitys.Category.StandardDocument;

@Service
@Qualifier("labresources")
public class BankStatementService implements BankStatementServiceIF {


    @Autowired
    private UserAccountRepository userAccountRepository;

    private final RestTemplate restTemplate;

    public BankStatementService(RestTemplate restTemplate) {
        this.restTemplate = restTemplate;
    }


    @Override
    public void requestBankStatement(String currentUserAccountID, BankStatement bankStatement) {

        Optional<UserAccount> optional = userAccountRepository.findById(currentUserAccountID);

        if(optional.isPresent()) {

            UserAccount userAccount = optional.get();

            DeliveryDTO deliveryDTO = new DeliveryDTO();
            DeliveryObjectDTO deliveryObjectDTO = new DeliveryObjectDTO();

            Address destination = new Address();
            destination.setName(userAccount.getUsername());
            destination.setStreetname(userAccount.getAddress().getStreet());
            destination.setHousenumber(userAccount.getAddress().getHouseNumber());
            destination.setPostcode(userAccount.getAddress().getPostCode());
            destination.setCity(userAccount.getAddress().getCity());
            destination.setCountry(userAccount.getAddress().getCountry());

            Address source = new Address();
            source.setName("BogerPay GmbH");
            source.setStreetname("Oberländerstraße");
            source.setHousenumber("14");
            source.setPostcode("93051");
            source.setCity("Regensburg");
            source.setCountry("Deutschland");

            deliveryDTO.setDestination(destination);
            deliveryDTO.setSource(destination);

            String currentUserIBAN = bankStatement.getBankStatementIBAN();
            deliveryDTO.setIbanBogerPay(currentUserIBAN);
            deliveryDTO.setPasswordBogerPay("1234");
            deliveryDTO.setUsernameBogerPay(userAccount.getEmail());

            deliveryDTO.setUsernameDeliveryGreim("boger@dg.de");
            deliveryDTO.setPasswordDeliveryGreim("1234");


            if("standard".equals(bankStatement.getDocumentOption())) {

                deliveryObjectDTO.setCategory(StandardDocument);
                deliveryObjectDTO.setDescription("bank statement");
                deliveryDTO.addDeliveryObjectDTOToList(deliveryObjectDTO);

            } else {

                deliveryObjectDTO.setCategory(LargeDocument);
                deliveryObjectDTO.setDescription("bank statement");
                deliveryDTO.addDeliveryObjectDTOToList(deliveryObjectDTO);

            }


            if("express".equals(bankStatement.getDeliverOption())) {
                deliveryDTO.setExpressdelivery(true);
            } else if ("special".equals(bankStatement.getDeliverOption())){
                deliveryDTO.setSpecialdelivery(true);
            }

            ResponseEntity<Long> success = restTemplate.postForEntity("http://im-codd.oth-regensburg.de:8834/api/deliver", deliveryDTO, Long.class);
            success.getStatusCode().is2xxSuccessful();

        }

    }

}
