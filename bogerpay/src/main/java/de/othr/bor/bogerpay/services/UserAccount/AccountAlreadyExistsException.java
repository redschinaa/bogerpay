package de.othr.bor.bogerpay.services.UserAccount;

public class AccountAlreadyExistsException extends Exception{

    AccountAlreadyExistsException(String errorMessage) {
        super(errorMessage);
    }

}
