package de.othr.bor.bogerpay.services.Transaction;

public class TransactionException extends Exception {

    public TransactionException(String errorMessage) {
        super(errorMessage);
    }

}
