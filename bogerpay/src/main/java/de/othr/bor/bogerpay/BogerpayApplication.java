package de.othr.bor.bogerpay;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class BogerpayApplication {

	public static void main(String[] args) {
		SpringApplication.run(BogerpayApplication.class, args);
	}

}
