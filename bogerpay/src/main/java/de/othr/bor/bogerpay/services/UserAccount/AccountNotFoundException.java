package de.othr.bor.bogerpay.services.UserAccount;

public class AccountNotFoundException extends RuntimeException {

    AccountNotFoundException(String errorMessage) {
        super(errorMessage);
    }

}
