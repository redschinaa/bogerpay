package de.othr.bor.bogerpay.entity;

import javax.persistence.*;
import javax.validation.constraints.NotNull;
import java.text.NumberFormat;
import java.util.Date;
import java.util.Locale;

@Entity
public class Transaction extends IdEntity {

    @NotNull
    private double amount;
    @Temporal(TemporalType.TIMESTAMP)
    private Date timestamp;
    private String reference;
    private String recipientName;

    @Transient
    private final NumberFormat nf = NumberFormat.getCurrencyInstance(Locale.GERMANY);

    public String getAmountFormatted(){
        return nf.format(amount / 1.0);
    }

    @ManyToOne
    private BankAccount consignor;

    @ManyToOne
    private BankAccount recipient;


    // Getter and Setter

    public double getAmount() {
        return amount;
    }

    public void setAmount(double amount) {
        this.amount = amount;
    }

    public Date getTimestamp() {
        return timestamp;
    }

    public void setTimestamp(Date timestamp) {
        this.timestamp = timestamp;
    }

    public String getReference() {
        return reference;
    }

    public void setReference(String reference) {
        this.reference = reference;
    }

    public String getRecipientName() {
        return recipientName;
    }

    public void setRecipientName(String recipientName) {
        this.recipientName = recipientName;
    }

    public BankAccount getConsignor() {
        return consignor;
    }

    public void setConsignor(BankAccount consignor) {
        this.consignor = consignor;
    }

    public BankAccount getRecipient() {
        return recipient;
    }

    public void setRecipient(BankAccount recipient) {
        this.recipient = recipient;
    }

}
