package de.othr.bor.bogerpay.entity;

import javax.persistence.Entity;

@Entity
public class BankStatement extends IdEntity {

    private String bankStatementIBAN;
    private String documentOption;
    private String deliverOption;


    // Getter and Setter

    public String getBankStatementIBAN() {
        return bankStatementIBAN;
    }

    public void setBankStatementIBAN(String bankStatementIBAN) {
        this.bankStatementIBAN = bankStatementIBAN;
    }

    public String getDocumentOption() {
        return documentOption;
    }

    public void setDocumentOption(String documentOption) {
        this.documentOption = documentOption;
    }

    public String getDeliverOption() {
        return deliverOption;
    }

    public void setDeliverOption(String deliverOption) {
        this.deliverOption = deliverOption;
    }

}
