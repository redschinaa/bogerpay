package de.othr.bor.bogerpay.dto;

import javax.persistence.Transient;
import java.io.Serializable;
import java.text.NumberFormat;
import java.util.Locale;

public class TransactionDTO implements Serializable {

    private double amount;
    private String recipientIBAN;
    private String recipientName;
    private String consignorIBAN;
    private String reference;

    // Set number format for german standard
    @Transient
    private final NumberFormat nf = NumberFormat.getCurrencyInstance(Locale.GERMANY);

    public String getAmountFormatted(){
        return nf.format(amount / 1.0);
    }


    // Getter and Setter

    public double getAmount() {
        return amount;
    }

    public void setAmount(double amount) {
        this.amount = amount;
    }

    public String getRecipientIBAN() {
        return recipientIBAN;
    }

    public void setRecipientIBAN(String recipientIBAN) {
        this.recipientIBAN = recipientIBAN;
    }

    public String getRecipientName() {
        return recipientName;
    }

    public void setRecipientName(String recipientName) {
        this.recipientName = recipientName;
    }

    public String getConsignorIBAN() {
        return consignorIBAN;
    }

    public void setConsignorIBAN(String consignorIBAN) {
        this.consignorIBAN = consignorIBAN;
    }

    public String getReference() {
        return reference;
    }

    public void setReference(String reference) {
        this.reference = reference;
    }

}
