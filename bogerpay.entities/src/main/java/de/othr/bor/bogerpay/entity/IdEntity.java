package de.othr.bor.bogerpay.entity;

import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.MappedSuperclass;
import java.util.Objects;

@MappedSuperclass
public abstract class IdEntity {

    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    private long Id;

    public long getId() {
        return Id;
    }

    @Override
    public int hashCode(){
        return Long.hashCode(Id);
    }

    @Override
    public boolean equals(Object o)
    {
        if (o == null) {
            return false;
        }
        if (getClass() != o.getClass()) {
            return false;
        }
        final IdEntity other = (IdEntity) o;
        if (!Objects.equals(Id, other.Id)) {
            return false;
        }
        return true;
    }

}
