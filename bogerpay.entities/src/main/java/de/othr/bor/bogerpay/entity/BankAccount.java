package de.othr.bor.bogerpay.entity;


import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Transient;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;
import java.text.NumberFormat;
import java.util.Locale;

@Entity
public class BankAccount {

    private double bankBalance;
    private long creditLimit = 0;
    private boolean archived = false;

    // Maximum size for german IBAN
    @Id
    @NotNull
    @Size(max = 22)
    private String IBAN;

    // Maximum size for german BIC
    @NotNull
    @Size(max = 11)
    private String BIC;

    // Set number format for german standard
    @Transient
    private final NumberFormat nf = NumberFormat.getCurrencyInstance(Locale.GERMANY);

    public String getBalanceFormatted(){
        return nf.format(bankBalance / 1.0);
    }



    // Getter and Setter

    public double getBankBalance() {
        return bankBalance;
    }

    public void setBankBalance(double bankBalance) {
        this.bankBalance = bankBalance;
    }

    public long getCreditLimit() {
        return creditLimit;
    }

    public void setCreditLimit(long creditLimit) {
        this.creditLimit = creditLimit;
    }

    public String getIBAN() {
        return IBAN;
    }

    public void setIBAN(String IBAN) {
        this.IBAN = IBAN;
    }

    public String getBIC() {
        return BIC;
    }

    public void setBIC(String BIC) {
        this.BIC = BIC;
    }

    public NumberFormat getNf() {
        return nf;
    }

    public boolean isArchived() {
        return archived;
    }

    public void setArchived(boolean archived) {
        this.archived = archived;
    }


}
